plot-misc.barchart module
-------------------------------------------

.. automodule:: plot_misc.barchart
   :members:

plot-misc.forest module
-------------------------------------------
.. automodule:: plot_misc.forest
   :members:

plot-misc.heatmap module
-------------------------------------------
.. automodule:: plot_misc.heatmap
   :members:

plot-misc.incidencematrix module
-------------------------------------------
.. automodule:: plot_misc.incidencematrix
   :members:

plot-misc.machine_learning module
-------------------------------------------
.. automodule:: plot_misc.machine_learning
   :members:

plot-misc.volcano module
-------------------------------------------
.. automodule:: plot_misc.volcano
   :members:

plot-misc.utils.utils module
-------------------------------------------
.. automodule:: plot_misc.utils.utils
   :members:

plot-misc.utils.formatting module
-------------------------------------------
.. automodule:: plot_misc.utils.formatting
   :members:

plot-misc.utils.colour module
-------------------------------------------
.. automodule:: plot_misc.utils.colour
   :members:
