.. plot-misc documentation master file, created by
   sphinx-quickstart on Tue Feb 13 20:43:34 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to plot-misc's documentation!
=====================================

Plot-misc combines a set of python-based functions and classes build on
top of matplotlib and (less frequently) seaborn. The module is designed 
to provide a fairly basic default illustration, returning figure 
and axes object to allow the user to customise the base illustration 
using regular matplotlib functionality. 

Content 
=======

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started


.. toctree::
   :maxdepth: 2
   :caption: Programmer reference
   
   api

.. toctree::
   :maxdepth: 3
   :caption: Examples
   
   examples

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
